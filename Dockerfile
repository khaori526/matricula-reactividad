FROM eclipse-temurin:17

MAINTAINER mitocode.com

COPY target/matricula-0.0.1-SNAPSHOT.jar matricula-0.0.1.jar

ENTRYPOINT ["java","-jar","/matricula-0.0.1.jar"]