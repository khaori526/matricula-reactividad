package com.daniel.matricula.controller;

import com.daniel.matricula.dto.EnrollmentDTO;
import com.daniel.matricula.model.Enrollment;
import com.daniel.matricula.pagination.PageSupport;
import com.daniel.matricula.service.IEnrollmentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.time.LocalDateTime;

import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;

@RestController
@RequestMapping("/enrollments")
@RequiredArgsConstructor
public class EnrollmentController {

    private final IEnrollmentService service;

    @Qualifier("defaultMapper")
    private final ModelMapper mapper;

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping
    public Mono<ResponseEntity<Flux<EnrollmentDTO>>> findAll() {
        Flux<EnrollmentDTO> fx = service.findAll().map(this::convertToDTO);

        return Mono.just(ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fx)
                )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/{id}")
    public Mono<ResponseEntity<EnrollmentDTO>> findById(@PathVariable("id") String id) {
        return service.findById(id)
                .map(this::convertToDTO)
                .map(e -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(e)
                )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping
    public Mono<ResponseEntity<EnrollmentDTO>> save(@RequestBody EnrollmentDTO dto, final ServerHttpRequest req) {
        Enrollment enrollment = this.convertToEntity(dto);
        enrollment.setDateEnrollment(LocalDateTime.now());
        return service.save(enrollment)
                .map(this::convertToDTO)
                .map(e -> ResponseEntity
                        .created(URI.create(req.getURI().toString().concat("/").concat(e.getId())))
                        .body(e)
                );
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/{id}")
    public Mono<ResponseEntity<EnrollmentDTO>> update(@RequestBody EnrollmentDTO dto, @PathVariable("id") String id) {
        return Mono.just(dto)
                .map(e -> {
                    e.setId(id);
                    return e;
                })
                .flatMap(e -> service.update(this.convertToEntity(e), id))
                .map(this::convertToDTO)
                .map(e -> ResponseEntity
                        .ok()
                        .body(e)
                )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Object>> delete(@PathVariable("id") String id) {
        return service.delete(id)
                .flatMap(result -> {
                    if(result){
                        return Mono.just(ResponseEntity.noContent().build());
                    }else{
                        return Mono.just(ResponseEntity.notFound().build());
                    }
                })
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    private EnrollmentDTO EnrollmentHateoas;

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/hateoas/{id}")
    public Mono<EntityModel<EnrollmentDTO>> getHateoas(@PathVariable("id") String id){
        Mono<Link> link = linkTo(methodOn(EnrollmentController.class).findById(id)).withSelfRel().toMono();

        return service.findById(id)
                .map(this::convertToDTO)
                .zipWith(link, EntityModel::of);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/pageable")
    public Mono<ResponseEntity<PageSupport<EnrollmentDTO>>> getPage(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "5") int size
    ){
        return service.getPage(PageRequest.of(page, size))
                .map(pageSupport -> {
                    PageSupport<EnrollmentDTO> dtoPageSupport = new PageSupport<>(
                            pageSupport.getContent().stream().map(this::convertToDTO).toList(),
                            pageSupport.getPageNumber(),
                            pageSupport.getPageSize(),
                            pageSupport.getTotalElements()
                    );
                    return ResponseEntity.ok()
                            .contentType(MediaType.APPLICATION_JSON)
                            .body(dtoPageSupport);
                })
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    private EnrollmentDTO convertToDTO(Enrollment model){
        return mapper.map(model, EnrollmentDTO.class);
    }

    private Enrollment convertToEntity(EnrollmentDTO dto){
        return mapper.map(dto, Enrollment.class);
    }
}
