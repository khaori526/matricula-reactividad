package com.daniel.matricula.repo;

import com.daniel.matricula.model.Student;

public interface IStudentRepo extends IGenericRepo<Student, String> {

}
