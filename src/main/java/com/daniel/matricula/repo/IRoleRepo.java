package com.daniel.matricula.repo;


import com.daniel.matricula.model.Role;

public interface IRoleRepo extends IGenericRepo<Role, String>{
}
