package com.daniel.matricula.repo;

import com.daniel.matricula.model.Enrollment;

public interface IEnrollmentRepo extends IGenericRepo<Enrollment, String> {

}
