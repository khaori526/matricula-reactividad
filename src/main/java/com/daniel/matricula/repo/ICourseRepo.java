package com.daniel.matricula.repo;

import com.daniel.matricula.model.Course;

public interface ICourseRepo extends IGenericRepo<Course, String> {

}
