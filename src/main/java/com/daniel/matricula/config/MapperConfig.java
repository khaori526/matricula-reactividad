package com.daniel.matricula.config;

import com.daniel.matricula.dto.CourseDTO;
import com.daniel.matricula.dto.StudentDTO;
import com.daniel.matricula.model.Course;
import com.daniel.matricula.model.Student;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;

@Configuration
public class MapperConfig {

    @Bean("defaultMapper")
    public ModelMapper defaultMapper() {
        return new ModelMapper();
    }

    @Bean("studentMapper")
    public ModelMapper studentMapper() {
        ModelMapper mapper = new ModelMapper();

        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        //Lectura
        TypeMap<Student, StudentDTO> typeMap1 = mapper.createTypeMap(Student.class, StudentDTO.class);
        typeMap1.addMapping(Student::getFirstname, (dest, v) -> dest.setFirstnameStudent((String) v));
        typeMap1.addMapping(Student::getLastname, (dest, v) -> dest.setLastnameStudent((String) v));
        typeMap1.addMapping(Student::getDni, (dest, v) -> dest.setDniStudent((String) v));
        typeMap1.addMapping(Student::getAge, (dest, v) -> dest.setAgeStudent((Integer) v));

        //Escritura
        TypeMap<StudentDTO, Student> typeMap2 = mapper.createTypeMap(StudentDTO.class, Student.class);
        typeMap2.addMapping(StudentDTO::getFirstnameStudent, (dest, v) -> dest.setFirstname((String) v));
        typeMap2.addMapping(StudentDTO::getLastnameStudent, (dest, v) -> dest.setLastname((String) v));
        typeMap2.addMapping(StudentDTO::getDniStudent, (dest, v) -> dest.setDni((String) v));
        typeMap2.addMapping(StudentDTO::getAgeStudent, (dest, v) -> dest.setAge((Integer) v));

        return mapper;
    }

    @Bean("courseMapper")
    public ModelMapper courseMapper() {
        ModelMapper mapper = new ModelMapper();

        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        //Lectura
        TypeMap<Course, CourseDTO> typeMap1 = mapper.createTypeMap(Course.class, CourseDTO.class);
        typeMap1.addMapping(Course::getName, (dest, v) -> dest.setNameCourse((String) v));
        typeMap1.addMapping(Course::getAcronym, (dest, v) -> dest.setAcronymCourse((String) v));
        typeMap1.addMapping(Course::getStatus, (dest, v) -> dest.setStatusCourse((Boolean) v));

        //Escritura
        TypeMap<CourseDTO, Course> typeMap2 = mapper.createTypeMap(CourseDTO.class, Course.class);
        typeMap2.addMapping(CourseDTO::getNameCourse, (dest, v) -> dest.setName((String) v));
        typeMap2.addMapping(CourseDTO::getAcronymCourse, (dest, v) -> dest.setAcronym((String) v));
        typeMap2.addMapping(CourseDTO::getStatusCourse, (dest, v) -> dest.setStatus((Boolean) v));

        return mapper;
    }

}
