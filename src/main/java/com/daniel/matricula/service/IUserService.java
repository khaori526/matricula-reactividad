package com.daniel.matricula.service;

import com.daniel.matricula.model.User;
import reactor.core.publisher.Mono;

public interface IUserService extends ICRUD<User, String>{
    Mono<User> saveHash(User user);
    Mono<com.daniel.matricula.security.User> searchByUser(String username);
}
