package com.daniel.matricula.service.impl;

import com.daniel.matricula.model.Course;
import com.daniel.matricula.repo.ICourseRepo;
import com.daniel.matricula.repo.IGenericRepo;
import com.daniel.matricula.service.ICourseService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CourseServiceImpl extends CRUDImpl<Course, String> implements ICourseService {

    private final ICourseRepo repo;

    @Override
    protected IGenericRepo<Course, String> getRepo() {
        return repo;
    }
}
