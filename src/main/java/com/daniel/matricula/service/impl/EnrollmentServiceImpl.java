package com.daniel.matricula.service.impl;

import com.daniel.matricula.model.Course;
import com.daniel.matricula.model.Enrollment;
import com.daniel.matricula.pagination.PageSupport;
import com.daniel.matricula.repo.ICourseRepo;
import com.daniel.matricula.repo.IEnrollmentRepo;
import com.daniel.matricula.repo.IGenericRepo;
import com.daniel.matricula.repo.IStudentRepo;
import com.daniel.matricula.service.IEnrollmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


@Service
@RequiredArgsConstructor
public class EnrollmentServiceImpl extends CRUDImpl<Enrollment, String> implements IEnrollmentService {

    private final IEnrollmentRepo repo;
    private final IStudentRepo studentRepo;
    private final ICourseRepo courseRepo;

    @Override
    protected IGenericRepo<Enrollment, String> getRepo() {
        return repo;
    }

    private Mono<Enrollment> populateStudent(Enrollment enrollment) {
        return studentRepo.findById(enrollment.getStudent().getId())
                .map(student -> {
                    enrollment.setStudent(student);
                    return enrollment;
                });
    }

    private Mono<Enrollment> populateCourses(Enrollment enrollment) {
        List<Mono<Course>> lst = enrollment.getCourses().stream()
                .map(item -> courseRepo.findById(item.getId())
                        .map(course -> {
                            item.setName(course.getName());
                            item.setAcronym(course.getAcronym());
                            item.setStatus(course.getStatus());
                            return item;
                        })
                ).toList();
        return Mono.when(lst).then(Mono.just(enrollment));
    }

    @Override
    public Mono<Enrollment> findById(String s) {
        return super.findById(s)
                .flatMap(this::populateStudent)
                .flatMap(this::populateCourses)
                .onErrorResume(e -> Mono.empty());
    }

    @Override
    public Flux<Enrollment> findAll() {
        return super.findAll()
                .flatMap(this::populateStudent)
                .flatMap(this::populateCourses);
    }

}
