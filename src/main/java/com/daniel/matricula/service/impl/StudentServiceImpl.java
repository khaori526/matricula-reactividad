package com.daniel.matricula.service.impl;

import com.daniel.matricula.model.Student;
import com.daniel.matricula.repo.IGenericRepo;
import com.daniel.matricula.repo.IStudentRepo;
import com.daniel.matricula.service.IStudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;


@Service
@RequiredArgsConstructor
public class StudentServiceImpl extends CRUDImpl<Student, String> implements IStudentService {

    private final IStudentRepo repo;

    @Override
    protected IGenericRepo<Student, String> getRepo() {
        return repo;
    }

    @Override
    public Flux<Student> findAllSortAsc() {
        Sort sort = Sort.by(Sort.Order.asc("age"));
        return getRepo().findAll(sort);
    }

    @Override
    public Flux<Student> findAllSortDesc() {
        Sort sort = Sort.by(Sort.Order.desc("age"));
        return getRepo().findAll(sort);
    }
}
