package com.daniel.matricula.service;


import com.daniel.matricula.model.Student;
import reactor.core.publisher.Flux;

public interface IStudentService extends ICRUD<Student, String>{

    Flux<Student> findAllSortAsc();
    Flux<Student> findAllSortDesc();
}
