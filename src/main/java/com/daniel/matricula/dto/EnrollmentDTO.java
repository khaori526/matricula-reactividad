package com.daniel.matricula.dto;

import com.daniel.matricula.model.Course;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EnrollmentDTO {

    private String id;
    private LocalDateTime dateEnrollment;
    private StudentDTO student;
    private List<CourseDTO> courses;
    private Boolean status;

}
