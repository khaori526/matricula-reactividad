package com.daniel.matricula.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StudentDTO {

    private String id;

    @NotNull(message = "El nombre es obligatorio")
    @NotEmpty(message = "El nombre es obligatorio")
    private String firstnameStudent;

    @NotNull(message = "El apellido es obligatorio")
    @NotEmpty(message = "El apellido es obligatorio")
    private String lastnameStudent;

    @NotNull(message = "El dni es obligatorio")
    @NotEmpty(message = "El dni es obligatorio")
    @Size(min = 8, max = 8, message = "El DNI debe tener exactamente 7 caracteres")
    private String dniStudent;

    @NotNull(message = "La edad es obligatoria")
    @Positive(message = "La edad debe ser un número entero positivo")
    @Digits(integer = 10, fraction = 0, message = "La edad debe ser un número entero positivo")
    private Integer ageStudent;

}
